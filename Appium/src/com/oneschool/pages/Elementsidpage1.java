package com.oneschool.pages;

import org.openqa.selenium.By;

public class Elementsidpage1 {
	
	
	// common
	
	public static By menuicon = By.className("android.widget.ImageView");
	
	
	
	// drawer icon

	
	public static By loginoptiondrawericon = By.xpath("//android.widget.TextView[@text = 'Login']");
	public static By logintextfield = By.id("com.school.kotlin:id/edit_text_phone");
	public static By passwordtextfield = By.id("com.school.kotlin:id/edit_text_password");
	public static By loginbutton = By.id("com.school.kotlin:id/button_sign_in");
	public static By logoutoptiondrawericon	= By.id("com.school.kotlin:id/text_logout");
	public static By searchnewschool = By.id("com.school.kotlin:id/text_search_school");
	
	// search
	
	
	
	public static By searchbyname = By.id("com.school.kotlin:id/edit_text_Search_schools");
	
	
	

}
