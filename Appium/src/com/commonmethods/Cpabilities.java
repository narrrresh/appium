package com.commonmethods;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;

public class Cpabilities {
	
	
	public static AndroidDriver  driver;
	
	public static AndroidDriver cpable() throws MalformedURLException
	
	{
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("deviceName", "Android");
		caps.setCapability("udid", "4e4bc60b"); //Give Device ID of your mobile phone
		caps.setCapability("platformName", "Android");
		caps.setCapability("platformVersion", "5.1.1");
		caps.setCapability("appPackage", "com.school.kotlin");
		caps.setCapability("appActivity", "com.school.kotlin.activities.LauncherActivity");
		caps.setCapability("noReset", "true");
		
		 driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), caps);
		
		return driver ;
		
	}

}
